const button = document.getElementById('button');
const color = document.querySelector('.color')

const hex = ['0','1', '2','3', '4','5', '6','7', '8','9','A', 'B', 'C', 'D', 'E', 'F']

button.addEventListener('click', () => {
   let colorStr = randomHex();
   document.querySelector('.wrapper').style.backgroundColor = colorStr;
   color.textContent = colorStr;
});

function randomHex() {
   let colorStr = '#';
   for (let i = 0; i < 6; i++) {
      colorStr += hex[getRandomColor()];
   }
   return colorStr;
}

function getRandomColor() {
   return Math.floor(Math.random() * hex.length);
}


