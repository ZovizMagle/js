const form = document.querySelector('#form');
const taskInput = document.querySelector('#taskInput')
const taskItems = document.querySelector('#taskItems')
const noTask = document.querySelector('.empty-container')
const clearAll = document.querySelector('#clear-tasks')

let tasks = [];

if (localStorage.getItem('tasks')) {
   tasks = JSON.parse(localStorage.getItem('tasks'))
   tasks.forEach(task => renderTask(task));
}

checkEmptyList()

form.addEventListener('submit', addTask);
taskItems.addEventListener('click', deleteTask);
taskItems.addEventListener('click', doneTask);
taskItems.addEventListener('click', editTask);
clearAll.addEventListener('click', clearTask);


function addTask(e) {
   e.preventDefault();
   const taskText = taskInput.value;
   const newTask = {
      id: Date.now(),
      text: taskText,
      done: false
   };

   tasks.push(newTask);
   saveToLocalStorage();

   renderTask(newTask)

   taskInput.value = '';
   taskInput.focus();

   checkEmptyList()
}

function doneTask(e) {
   if (e.target.dataset.action !== 'done') return;

      const parentNode = e.target.closest('.list-item');

      const id = Number(parentNode.id);
      
      const task = tasks.find(task => task.id === id)
      task.done = !task.done;
      saveToLocalStorage();
   
      const taskTitle = parentNode.querySelector('.task-title');
      taskTitle.classList.toggle('task-title--done');
}

function deleteTask(e) {
   if (e.target.dataset.action !== 'delete') return;
      const parentNode = e.target.closest('.list-item');
      const id = Number(parentNode.id);
      tasks = tasks.filter(task => task.id !== id);
      saveToLocalStorage();
      parentNode.remove();
      checkEmptyList();
}

function checkEmptyList() {
   if (tasks.length === 0) {
      const emptyListHTML = document.createElement('div')
      emptyListHTML.innerHTML = `<div class="empty-list" id="noTask">
                                    <img class="no-task__img" src="./img/no-task.png" alt="no-task"/>
                                    <div class="no-task__title">To-do list is empty</div>
                                 </div>`
      noTask.appendChild(emptyListHTML)
   }

   if (tasks.length > 0) {
      const emptyListEl = document.querySelector('.empty-list');
      emptyListEl ? emptyListEl.remove() : null;
   }
}

function saveToLocalStorage() {
   localStorage.setItem("tasks", JSON.stringify(tasks))
}

function renderTask(task) {
      const cssClass = task.done ? "task-title task-title--done" : "task-title";
   const taskHTML = `<li id="${task.id}" class="list-item">
                        <div class="text-wrapp">
                           <button class="button-done" type="button" data-action="done" >
                              <img src="./img/done.svg" alt="done" class="img-done">
                           </button>   
                           <span class="${cssClass}" id="text">${task.text}</span>
                        </div>
                        <div class="task-item__buttons">
                           <button class="button-edit" type="button" data-action="edit">
                              <img src="./img/edit.png" alt="edit" class="img-edit">
                           </button>
                           <button class="button-trash" type="button" data-action="delete">
                              <img src="./img/trash.png" alt="trash" class="img-trash">
                           </button>
                        </div>
                     </li>`
      taskItems.insertAdjacentHTML('beforeend', taskHTML);
}

function editTask(e) {
   if (e.target.dataset.action) {
      if (e.target.dataset.action === 'edit') {
         let parentNode = e.target.closest('.list-item');
         let taskTitle = parentNode.querySelector('.task-title');
         const image = parentNode.querySelector('.img-edit');
         const id = Number(parentNode.id);
         const task = tasks.find(task => task.id === id);
         taskTitle.innerHTML = `<input class='edit-input' type='text' value='${task.text}'>`;
         e.target.dataset.action = 'save';
         image.src = './img/diskette.png'
      } else {
         let parentNode = e.target.closest('.list-item');
         let inputValue = parentNode.querySelector('.edit-input').value;
         const image = parentNode.querySelector('.img-edit');
         const id = Number(parentNode.id);
         const task = tasks.find(task => task.id === id);
         task.text = inputValue;
         saveToLocalStorage();
         e.target.dataset.action = 'edit';
         image.src = './img/edit.png'
      }
   }
}

function clearTask() {
   tasks = [];
   saveToLocalStorage();
   taskItems.remove();
   if (noTask) {
      const emptyListEl = document.querySelector('.empty-list');
      emptyListEl ? emptyListEl.remove() : null;
      checkEmptyList();
   }
}
