const menu = [
   {
      id: 1,
      title: "Ice-cream",
      category: "desserts",
      price: 5.50,
      img: "./img/ice-cream.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 2,
      title: "Burger",
      category: "lanch",
      price: 15.99,
      img: "./img/burger.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 3,
      title: "Spaghetti",
      category: "dinner",
      price: 13.99,
      img: "./img/spaghetti.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 4,
      title: "Fries",
      category: "lanch",
      price: 7.35,
      img: "./img/fries.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 5,
      title: "Stake",
      category: "dinner",
      price: 45.35,
      img: "./img/steak.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 6,
      title: "Toast",
      category: "breakfast",
      price: 6.75,
      img: "./img/toast.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 7,
      title: "Khachapuri",
      category: "breakfast",
      price: 8.35,
      img: "./img/khachapuri.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 8,
      title: "Pancakes",
      category: "breakfast",
      price: 7.99,
      img: "./img/pancakes.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 9,
      title: "Borsch",
      category: "lanch",
      price: 13.40,
      img: "./img/ukrainian.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 10,
      title: "Risotto",
      category: "dinner",
      price: 13.40,
      img: "./img/risotto.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 11,
      title: "Cake Srtawberry",
      category: "desserts",
      price: 6.45,
      img: "./img/cake-srtawberry.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 12,
      title: "Chocolate Cake",
      category: "desserts",
      price: 9.45,
      img: "./img/chocolate-cake.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 13,
      title: "Berry shake",
      category: "shake",
      price: 3.45,
      img: "./img/berry-shake.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 14,
      title: "Strawberry shake",
      category: "shake",
      price: 3.45,
      img: "./img/strawberry-shake.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 15,
      title: "Raspberry shake",
      category: "shake",
      price: 3.45,
      img: "./img/raspberry-shake.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
   {
      id: 16,
      title: "Caesar salad",
      category: "breakfast",
      price: 10.45,
      img: "./img/salat.jpg",
      desc: `Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime quis mollitia quisquam numquam? Autem, sed at maxime sit alias pariatur perspiciatis iure voluptates, ullam enim tenetur non a, vel ea!`
   },
];

const menuContainer = document.querySelector('.menu-container');
const filterBtns = document.querySelectorAll('.button');


window.addEventListener('DOMContentLoaded', () => {
   displayMenuItems(menu);
});

filterBtns.forEach(btn => {
   btn.addEventListener('click', e => {
      const category = e.currentTarget.dataset.id;
      const menuCategory = menu.filter(menuItem => {
         if (menuItem.category === category) {
            return menuItem;
         }
      });
      if (category === 'all') {
         displayMenuItems(menu);
      } else {
         displayMenuItems(menuCategory);
      }
   })
})





function displayMenuItems(menuItems) {
   let displayMenu = menuItems.map(item => {
      return `<div class="menu">
      <div class="menu-item">
         <img src=${item.img} class="food-photo" alt=${item.title}>
         <div class="item-wrapp">
            <div class="item-info">
               <h4 class="item-title">${item.title}</h4>
               <p class="price">$${item.price}</p>
            </div>
            <p class="menu-text">${item.desc}</p>
         </div>
      </div>
   </div>`;
   });
   displayMenu = displayMenu.join("");
   menuContainer.innerHTML = displayMenu;

}