class Validator {
   constructor(name, email, password, confirmPassword, form) {
      this.name = document.getElementById(name);
      this.email = document.getElementById(email);
      this.password = document.getElementById(password);
      this.confirmPassword = document.getElementById(confirmPassword);
      this.form = document.getElementById(form);
      this.isRequired = (value) => (value === "" ? false : true);
      this.isBetween = (length, min, max) => length < min || length > max ? false : true;
      this.isWhiteSpace = (s) => {return s.indexOf(' ') >= 0;} 
   }

   showError(input, message) {
      const formGroupReverse = input.parentElement;
      formGroupReverse.classList.remove("success");
      formGroupReverse.classList.add("error");

      const error = formGroupReverse.querySelector(".error-message");
      error.textContent = message;
   }

   showSuccess(input) {
      const formGroupReverse = input.parentElement;
      formGroupReverse.classList.remove("error");
      formGroupReverse.classList.add("success");

      const error = formGroupReverse.querySelector(".error-message");
      error.textContent = "";
   }

   isPasswordSecure(password) {
      const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})/;
      return re.test(password);
   }

   isEmailValid(email) {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
   }

   

   checkName() {
      let isValid = false;
      const minLength = 3, maxLength = 15, nameValue = this.name.value;

      if (!this.isRequired(nameValue)) {
         this.showError(this.name, "Forname cannot be blank.");
      } else if (!this.isBetween(nameValue.length, minLength, maxLength)) {
         this.showError(this.name, `Forname must be between ${minLength} and ${maxLength} characters.`);
      } else if (this.isWhiteSpace(nameValue)) {
         this.showError(this.name, 'Whitespace cannot be used')
      } else {
         this.showSuccess(this.name);
         isValid = true;
      }

      return isValid;
   }

   checkEmail() {
      let isValid = false;
      const emailValue = this.email.value;

      if (!this.isRequired(emailValue)) {
         this.showError(this.email, "Email cannot be blank.");
      }else if (this.isWhiteSpace(emailValue)) {
         this.showError(this.email, 'Whitespace cannot be used')
      } else if (!this.isEmailValid(emailValue)) {
         this.showError(this.email, "Email is not valid.");
      } else {
         this.showSuccess(this.email);
         isValid = true;
      }

      return isValid;
   }

   checkPassword() {
      let isValid = false;
      const passwordValue = this.password.value;

      if (!this.isRequired(passwordValue)) {
         this.showError(this.password, "Password cannot be blank.");
      } else if (this.isPasswordSecure(passwordValue)) {
         this.showError(this.password, "Password must has at least 6 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character in (!@#$%^&*)");
      } else if (this.isWhiteSpace(passwordValue)) {
         this.showError(this.password, 'Whitespace cannot be used')
      } else {
         this.showSuccess(this.password);
         isValid = true;
      }

      return isValid;
   }

   checkConfirmPassword() {
      let isValid = false;
      const confirmPasswordValue = this.confirmPassword.value,
         passwordValue = this.password.value;
      if (!this.isRequired(confirmPasswordValue)) {
         this.showError(this.confirmPassword, "Confirm password cannot be blank.")
      }else if (passwordValue !== confirmPasswordValue) {
         this.showError(this.confirmPassword, "Confirm password does not match");
      } else if (this.isWhiteSpace(confirmPasswordValue)) {
         this.showError(this.confirmPassword, 'Whitespace cannot be used')
      } else {
         this.showSuccess(this.confirmPassword);
         isValid = true;
      }
      return isValid;
   }
   }

document.getElementById("button").addEventListener("click", (e) => {
   e.preventDefault();

   const validator = new Validator("input-name", "email", "password", "confirm-password", "form");

   let validatorCheckName = validator.checkName();
   let validatorCheckEmail = validator.checkEmail();
   let validatorCheckPassword = validator.checkPassword();
   let validatorCheckConfirmPassword = validator.checkConfirmPassword();


   let isFormValid = validatorCheckName && validatorCheckEmail &&
   validatorCheckPassword && validatorCheckConfirmPassword;
   
   alert(isFormValid ? "validation success" : "Check form again");
});


function showValue(newValue) {
   document.getElementById("value").innerHTML = newValue;
}







