const weatherCurrentContainer = document.querySelector('.weather__container')
const search = document.querySelector('.search')
const weekContainer = document.createElement('div');
weekContainer.classList.add('weather__for-week-container')
const selectedValue = document.querySelector('.search__selected')
let city = 'Kyiv'


document.addEventListener("DOMContentLoaded", function () {
  forWeekWeather();
  loadCurrentWeather();
})


document.addEventListener('keydown', e => {
  if (e.key === 'Enter') {
    let value = search.value.trim();
    if (!value) {
      return false;
    }
    city = value;
    loadCurrentWeather();
    forWeekWeather();
  }
})

async function loadCurrentWeather() {
  let city = search.value.trim();
  weatherCurrentContainer.innerHTML = `
    <div class='weather__loading'>
      <img class='weather__loading-img' src='./img/loader.gif'>
    </div>
  `
  const api = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=aa5ad92bcb56044fe3152ee1852b589c&units=metric`
  const response = await fetch(api);
  const responseResult = await response.json();
  selectedValue.innerText = search.value
  if (response.ok) {
    getCurrentWeather(responseResult);
  } else {
    weatherCurrentContainer.innerHTML = responseResult.message;
  }
}

function getCurrentWeather(data) {
  const location = data.name;
  const temp = Math.round(data.main.temp);
  const feelsLike = Math.round(data.main.feels_like);
  const description = data.weather[0].description;
  const icon = data.weather[0].icon;

  const renderCurrentWeather = `
  <div class="weather__current-day">
    <div class="current-day__temperature">
        <p class="temperature-main">${temp}&deg;C</p>
        <p class="feels-like-main">Feels like ${feelsLike}&deg;C</p>
    </div>

    <div class="current-day__city">
        <p class="clouds-main">${description}</p>
        <p class="city-main">${location}</p>
    </div>
        <img class="current-day__img" src="http://openweathermap.org/img/w/${icon}.png" alt="${description}"/>
  </div>
  `
  weatherCurrentContainer.innerHTML = renderCurrentWeather;
}





async function forWeekWeather() {
  let city = search.value.trim();
  const api = `https://api.openweathermap.org/data/2.5/forecast?q=${city}&cnt=36&appid=aa5ad92bcb56044fe3152ee1852b589c&units=metric`
  const response = await fetch(api);
  const responseResult = await response.json();
  if (response.ok) {
    getForWeekWeather(responseResult)
  } 
}


function getDays(dataDays) {
  let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  let date = new Date(dataDays)
  let day = date.getDay()
  return days[day]
}




function getForWeekWeather(data) {
  let lists = data.list;
  let filterTime = lists.filter((time) => time.dt_txt.includes('12:00:00'))
  let renderForWeekWeather = filterTime.map(item => {
    return  `
              <div class="weather__for-week">
                <div class="for-week__block">
                    <p class="day">${getDays(item.dt_txt)}</p>
                    <img class="for-week" src="http://openweathermap.org/img/w/${item.weather[0].icon}.png" alt="${item.weather[0].description}"/>
                    <p class="clouds">${item.weather[0].description}</p>
                    <div class="temperature-block">
                      <p class="temperature">${Math.round(item.main.temp)}&deg;C</p>
                      <p class="feels-like">${Math.round(item.main.feels_like)}&deg;C</p>
                    </div>
                </div>
              </div>
            `
  })
  renderForWeekWeather = renderForWeekWeather.join('');
  weekContainer.innerHTML = renderForWeekWeather;
  weatherCurrentContainer.appendChild(weekContainer)
}